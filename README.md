Role Name
=========

role to generate nagiox server config from playbook host

Requirements
------------

use nrpe module to config nagios monitor servers first
Role Variables
--------------

at the minimum you need to define the group name of the monitoring hosts
Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

GPL

Author Information
------------------
bing
